package myutil;

import java.util.*;

public class Point {
	public float obj_id, sys_id, exist_p, sky_prob = 1, gsky_prob = 1; //object id, system id, existential probability and skyline probability.
	int loc_id; //local system id.
	public float []attr; //attributes of the point.
	Point(int n){
		attr = new float[n];
	}
}
