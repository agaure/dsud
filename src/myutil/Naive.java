package myutil;

import java.util.*;
import myutil.*;

public class Naive {
	public static ArrayList<Point> skylines(ArrayList<Point> data, Query query){
		//ArrayList<Integer> dimen = data.query.dimen;
		ArrayList<Point> SkyLineSet = new ArrayList<Point>();
		boolean isSkyline;
			//Start Timer-------------------
		int sz = data.size(); 
		for(int i = 0; i < sz; i++){
			isSkyline = true;
			for(int j = 0; j < sz; j++){
				if(Domin.isDominate(data.get(i).attr, data.get(j).attr)){
					data.get(i).sky_prob = upSkyProb(data.get(i).sky_prob, data.get(j).exist_p);
					if(query.q > data.get(i).sky_prob){
						isSkyline = false;
						break;
					}
				}
				//
				/*if(data.get(i).obj_id == 94){
					System.out.print(data.get(j).obj_id + " " + data.get(i).sky_prob + "     ");
				}*/

			}
			if(isSkyline && (data.get(i).sky_prob >= query.q)){
				SkyLineSet.add(data.get(i));
			}
		}
		return SkyLineSet;
	}
	public static float upSkyProb(float a, float b){
		a = a*(1-b); 
		return a;
	}
}
