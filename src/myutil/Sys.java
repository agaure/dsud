package myutil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import myutil.*;

public class Sys {
	public boolean imDone = false; //if skyline Set is empty.
	public int size = 0; //size of no of data elements.
	public Query query = null;
	public Point chosenOne = null; //sorting hat.
	public ArrayList<Point> data = new ArrayList<Point>(); 
	public ArrayList<Point> Sky = null;
	Sys(Query q){
		this.query = q; 
	}
	//compute and send skyline.
	public Point sendRep(){
		updateData();
		Point pt = chooseone();
		return pt;
	}	
	
	//compute skyline.
	public void skylines(){
		Sky = Naive.skylines(data, query);
	}
	//sort all skylines
	public void updateData(){
		Collections.sort(Sky, new Comparator<Point>() {
            public int compare(Point point1, Point point2) {
                return Float.compare(point2.sky_prob, point1.sky_prob);
            }
        });
	}
	//delete the chosen one.
	public Point chooseone(){
		Point one = null;
		if(!Sky.isEmpty()){
			one = Sky.get(0);
			Sky.remove(0);
			if(Sky.isEmpty()){ //check if skyline set is empty after removal.
				imDone = true;
			}
		}
		return one;
	}
	//probability that the candidate is skyline after passing through this system. 
	public Point decideFate(){
		int sz = Sky.size();
		for(int i = 0; i < sz; i++){
			if(Domin.isDominate(chosenOne.attr, Sky.get(i).attr)){ //Chosen-one is dominated by tuple in current system.
				chosenOne.sky_prob = chosenOne.sky_prob*(1-Sky.get(i).exist_p); 
				}
		}
		for(int i = 0; i < Sky.size(); i++){
			if(Domin.isDominate(Sky.get(i).attr, chosenOne.attr)){ //chosen tuple dominates others.
				Sky.get(i).sky_prob = Sky.get(i).sky_prob*(1-chosenOne.exist_p); 
				if(Sky.get(i).sky_prob < query.q){ // remove if it's probability is lesser than q.
					Sky.remove(i);
					i--;
					if(Sky.isEmpty()){ //check if Skyline set is empty.
						imDone = true;
					}
				}
			}
		}
		return chosenOne;
	}
	
	//------------------
	
	public void eskylines(){
		Sky = eNaive.skylines(data, query);
	}
	
	public Point decideFateG(){
		
		for(int i = 0; i < Sky.size(); i++){
			if(Domin.isDominate(Sky.get(i).attr, chosenOne.attr)){ //chosen tuple dominates others.
				Sky.get(i).sky_prob = Sky.get(i).sky_prob*(1-chosenOne.exist_p);
				if(query.q > Sky.get(i).sky_prob*(chosenOne.sky_prob/chosenOne.exist_p)){
					Sky.get(i).sky_prob = Sky.get(i).sky_prob*(chosenOne.sky_prob/chosenOne.exist_p);
				}
				if(Sky.get(i).sky_prob < query.q){ // remove if it's probability is lesser than q.
					Sky.remove(i);
					i--;
					if(Sky.isEmpty()){ //check if Skyline set is empty.
						imDone = true;
					}
				}
			}
		}
		int sz = Sky.size();
		for(int i = 0; i < sz; i++){
			if(Domin.isDominate(chosenOne.attr, Sky.get(i).attr)){ //Chosen-one is dominated by tuple in current system.
				chosenOne.sky_prob = chosenOne.sky_prob*(1-Sky.get(i).exist_p); 
				}
		}

		return chosenOne;
	}
	
}
