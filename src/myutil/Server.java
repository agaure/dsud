package myutil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import myutil.*;

//Central Node in Network.
public class Server extends Sys {
	Server(Query query){
		super(query); //call constructor of system class.
	}
	//choose best to broadcast.
	public Point getBest(){ 
		updateData();
		Point one = chooseone(); // select the chosen one.
		return one;
	}
	//broadcasting implementation.
	public Network broadcast(Point one, Network network){
		if(one == null)
			System.out.print(null+"\n");
		for(int i = 1; i <= network.no_sys; i++){
			if(one.sys_id != i && !network.net.get(i).imDone){
				network.net.get(i).chosenOne = one; //place sorting hat on candidate. 
				//network.no_msg++;
			}
		}
		network.no_msg++;
		return network;
	}
	//check if skylines from all nodes are present.
	public boolean ifAllAre(int no_sys){
		int[] pres = new int[no_sys+1];
		for(Point p : Sky){
			pres[(int)p.sys_id] += 1;
		}
		boolean flag = true;
		for(int x : pres){
			if(x == 0){
				flag = flag & false; 
			}
		}
		return flag;
	}
	//check if point from corresponding system is present 
	public boolean isPres(int sys){
		boolean flag = false;
		for(Point p : Sky){
			if(p.sys_id == sys){
				flag = true;
			}
		}
		return flag;
	}
		
	//demand more points if points from corresponding tuples got pruned.
	public void demandMore(){
		//int[]
	}
	
	//---------------------
	public Point getBestG(){ 
		updateDataG();
		Point one = chooseone(); // select the chosen one.
		return one;
	}
	public void updateDataG(){
		Collections.sort(Sky, new Comparator<Point>() {
            public int compare(Point point1, Point point2) {
                return Float.compare(point2.gsky_prob, point1.gsky_prob);
            }
        });
	}
}
