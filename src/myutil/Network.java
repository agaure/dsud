package myutil;

import java.util.*;

public class Network {
	public int no_sys, no_pts, no_dim; //no. of systems, points and diamentions of data.
	public int no_msg = 0; // no. of messages sent.
	public ArrayList<Sys> net = new ArrayList<Sys>(); //the list of systems in network.
	public ArrayList<Point> Skylines = new ArrayList<Point>(); // Final Skyline Set.
	Network(int[] info, Query query){ 
		no_sys = info[0];
		no_pts = info[1];
		no_dim = info[2];
		net.add(new Server(query));
		for(int i = 1; i <= no_sys; i++){
			net.add(new Sys(query)); // create system objects.
		}
	}	
}
