package myutil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.io.*;

public class Impure {
	public static Network getData(String path) throws IOException{
		int[] info = getdataInfo(path);
		Query query = getQuery(path);
		Network network = new Network(info, query);
		network = getsysdata(path, network);
		return network;
	}
	public static Query getQuery(String path) throws IOException{
		Scanner in = null;
		Query query = new Query();
		try{
			in = new Scanner(new File(path+"dist_query.txt"));
			while(in.hasNextFloat()){
				query.q = in.nextFloat();
			}
		}
		finally{
			if(in != null){
				in.close();
			}
		}
		return query;
	}
	public static int[] getdataInfo(String path) throws IOException{
		int[] info = new int[3];
		Scanner in = null;
		try{
			in = new Scanner(new File(path+"dist_aux2.txt"));
			int i = 0;
			while(in.hasNextInt()){
				info[i] = in.nextInt();
				i++;
			}
		}
		finally{
			if(in != null){
				in.close();
			}
		}
		return info;
	}
	public static Network getsysdata(String path, Network network) throws IOException{
		Scanner in = null;
		//System.out.print(network.no_dim+" "+ network.no_pts +" "+ network.no_sys+" ");
		try{
			in = new Scanner(new File(path+"dist_data2.txt"));
			while(in.hasNextLine()){
				Point pt = new Point(network.no_dim);
				String[] textRow = (in.nextLine()).split("\\s+");
				float[] row = new float[textRow.length];
				for(int i = 0; i < textRow.length; i++){
					//System.out.print("\n"+textRow[i]+"-");
					row[i] = Float.parseFloat(textRow[i]);
				}
				//System.out.print(row[1]+"\n");
				pt.sys_id = (int)row[0];
				pt.obj_id = row[1];
				pt.exist_p = row[row.length-1];
				pt.sky_prob = pt.exist_p;
				for(int i = 0; i < network.no_dim; i++){
					pt.attr[i] = row[i+2];
				}
				int ind = (network.net.get((int)pt.sys_id)).data.size();
				pt.loc_id = ind;
				(network.net.get((int)pt.sys_id)).data.add(pt);
				(network.net.get((int)pt.sys_id)).size++;
			}
		}
		finally{
			if(in != null){
				in.close();
			}
		}
		return network;
	}
}