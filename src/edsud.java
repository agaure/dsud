import myutil.*;

import java.util.*;
import java.io.*;
import java.util.Arrays;


public class edsud {
	public static void main(String args[]){
		String path = "data/";
		Query query = new Query(); 
		Network network = null; 
		try{
			query = Impure.getQuery(path); //get query. (min prob) 
			network = Impure.getData(path); //get network.
		}
		catch(IOException e){
			System.out.println(e); 
		}
		
		network = phase1(network); // Each node sends it's representative. 
		Server central = (Server)network.net.get(0);
		central.eskylines(); // Skyline calculated on Central system.
		
		while(!allDone(network) || !network.net.get(0).Sky.isEmpty()){  //loop till either all nodes are null or central is not empty.
			Point one = phase2((Server)network.net.get(0)); //choose best one at central.		
			network = phase3(network, one); //broadcast the best one.
			network = phase4(network, one); //decide fate.
			network = sendAgain(network, query);
			//System.out.print(network.net.get(0).Sky.size()+" ");
		}
		System.out.println("eSkyline size: " + network.Skylines.size());
		System.out.println("No. of Message: " + network.no_msg);
		System.out.println("No. of Bytes: " + network.no_msg*4);
		Collections.sort(network.Skylines , new Comparator<Point>() {
            public int compare(Point point1, Point point2) {
                return Float.compare(point1.obj_id, point2.obj_id);
            }
        });
		/*for(Point p : network.Skylines){
			System.out.print((int)p.obj_id + " ");
		}*/
	}
	//Collect representatives from all systems.
	static Network phase1(Network network){
		for(int i = 1; i <= network.no_sys && !network.net.get(i).imDone; i++){
			network.net.get(i).skylines(); //compute skylines.
			network.net.get(0).data.add(network.net.get(i).sendRep()); //sending skyline of local to central.
			network.no_msg++;
		}
		return network;
	}
	//choose best one at Central.
	static Point phase2(Server central){
		Point one = central.getBest();
		return one;
	}
	 //Broadcasting.
	static Network phase3(Network network, Point one){
		Server central = (Server)network.net.get(0); //get central node in network.
		central.broadcast(one, network); //broadcast to all systems.
		//central.updateData(one); //delete this point from server
		return network;
	}
	//decide fate for chosen one.
	static Network phase4(Network network, Point one){
		boolean isSky = true;
//		Point one = null;
		for(int i = 1; i <= network.no_sys; i++){ //for all systems.
			if(network.net.get(i).chosenOne != null){ //if the one was sent to it.
				one = network.net.get(i).decideFateG(); //cross check for domination and update probability and skyline set accordingly.
				network.net.get(i).chosenOne = null; 
				if(one.sky_prob < network.net.get(i).query.q){ //if it's still a skyline.
					isSky = false;
					//break;
				}
			}
		}
		if(isSky && one != null){ //add if it's a skyline after going through all nodes.
			network.Skylines.add(one);
		}
		return network;
	}
	
	//to check if all nodes are finished sending skyline.
	static boolean allDone(Network network){
		boolean flag = true;
		//System.out.print(" ]");
		for(int i = 1; i <= network.no_sys; i++){
			//System.out.print("\n"+ network.net.get(i).imDone);
			if(!network.net.get(i).imDone){
				flag = flag && false;
			}
		}
		return flag; 
	}
	//Sending at phase2
	static Network sendAgain(Network network, Query query){
		for(int i = 1; i <= network.no_sys; i++){
			if(!((Server)network.net.get(0)).isPres(i) && (!network.net.get(i).imDone)){ //check if system of chosen-one is not empty.  
				Point pt = network.net.get(i).sendRep(); //send next candidate from that system.
				network.no_msg++; //increase the count for msgs sent.
				network.net.get(0).chosenOne = pt; //put new candidate on test-sit.
				pt = network.net.get(0).decideFateG(); // evaluate it's probability and it's dominated candidates.
				network.net.get(0).chosenOne = null;
				if(pt.sky_prob >= query.q){ //if still good add to central Skyline set. 
					network.net.get(0).Sky.add(pt);
				}
			}
			
		}
		return network;
	}
}